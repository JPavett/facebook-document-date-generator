Facebook Document Date Generator

A tools to add creation timestamps to audio files, photos and videos downloaded through Facebook's "Download your information" tool. This is needed as Facebook strips all metadata from these images, and this information is currently lost otherwise.

Copy the relevant directories from the download into the data/messages and data/documents directories, and the creation / taken timestamps will be set to the timestamp from the message where the document was initially sent.


`node index.js`