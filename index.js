const { glob } = require("glob")
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require("fs"));
var { utimes } = require("utimes");

const readJSONFiles = async () => {
  const messagesFiles = glob.sync(`data/messages/**/message_*.json`)
  console.log(messagesFiles)
  const promises = [];
  messagesFiles.forEach(mFile => {
    promises.push(fs.readFileAsync(mFile, 'utf8'));
  })

  return Promise.all(promises);
}

readJSONFiles().then(result => {
  const map = {};
  result.forEach(data => {
    const messagesContents = JSON.parse(data);

    messagesContents.messages
      .forEach(m => {
        if (m.photos) {
          m.photos.filter(p => {
            const splitted = p.uri.split("/")
            const messagePhotoFileName = splitted[splitted.length - 1];
            map[messagePhotoFileName] = m.timestamp_ms;
          })
        }

        if (m.audio_files) {
          m.audio_files.filter(a => {
            const splitted = a.uri.split("/")
            const messagePhotoFileName = splitted[splitted.length - 1];
            map[messagePhotoFileName] = m.timestamp_ms;
          })
        }

        if (m.videos) {
          m.videos.filter(v => {
            const splitted = v.uri.split("/")
            const messagePhotoFileName = splitted[splitted.length - 1];
            map[messagePhotoFileName] = m.timestamp_ms;
          })
        }
      })
  })

  fs.writeFileSync("./map.json", JSON.stringify(map))
}).then(() => {
  fs.readFileAsync("./map.json", 'utf8').then(data => {
    const map = JSON.parse(data);
    const filePath = glob.sync(`data/documents/**/*.{jpg,mp4}`)

    filePath.forEach(file => {

      const [, , , , photo] = file.split("/");

      if (map[photo]) {
        console.log(photo)
        utimes(file, {
          btime: map[photo],
          atime: map[photo],
          mtime: map[photo]
        });
      }
    })
  })
});
